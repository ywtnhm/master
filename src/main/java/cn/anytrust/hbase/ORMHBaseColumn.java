package cn.anytrust.hbase;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 用以描述字段所属的 family与qualifier. 也就是hbase的列与列簇.
 * 唯一特殊的是rowkey.只有rowkey的值  family与qulifier都是rowkey.最终的判断方案在HBaseUtils中进行.
 * FIXME 当然,该方案不合理.需要进行修改.
 * @author gaoweigong
 * @createTime 2016-01-28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD })
@Inherited
public @interface ORMHBaseColumn {
	
	public String family() default "";
	public String qualifier() default "";
	public boolean timestamp() default false;
}
